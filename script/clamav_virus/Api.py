import flask
import subprocess
from flask import request, jsonify
import json
import ast
import config as cf

app = flask.Flask(__name__)
app.config["DEBUG"] = True
data = [
    
        'url','post'
    
]
url= cf.url
paths_quick_scan = cf.paths_quick_scan
save_path = cf.save_path

@app.route('/api/quick_scan', methods=['GET'])
def quick_scan():
	paths_quick_scan = cf.paths_quick_scan
	if 'style' in request.args:
		style = int(request.args['style'])
	else:
		return "Error: No style field provided."
	if style == 1:
		type = r"--remove"
	else:
		type = r"--move="+"\""+save_path+"\""
	return_value=""
	for path in paths_quick_scan:
		cmd = url+path+type
		returned_output = subprocess.check_output(cmd)
		return_value= return_value+returned_output.decode("utf-8")
	return return_value
  

@app.route('/api/all_scan', methods=['GET'])
def all_scan():
	if 'paths' in request.args:
		paths = request.args['paths']
	else:
		return "Error: No paths field provided."
	if 'style' in request.args:
		style = int(request.args['style'])	
	else:
		return "Error: No style field provided. Please specify an id."
	if style == 1:
		type = r"--remove"
	else:
		type = r"--move="+"\""+save_path+"\""
	paths =paths.replace("\"", "")
	paths = paths.split(",")
	return_value=""
	for path in paths:
		cmd = url+" "+path+" "+type
		returned_output = subprocess.check_output(cmd)
		return_value= return_value+returned_output.decode("utf-8")
	return return_value
@app.route('/api/selective_scan', methods=['GET'])

def selective_scan():
	if 'paths' in request.args:
		paths = request.args['paths']
	else:
		return "Error: No paths field provided."
	if 'style' in request.args:
		style = int(request.args['style'])
	else:
		return "Error: No style field provided."
	if style == 1:
		type = r"--remove"
	else:
		type = r"--move="+"\""+save_path+"\""
	paths =paths.replace("\"", "")
	paths = paths.split(",")
	return_value=""
	for path in paths:
		cmd = url+" "+path+" "+type
		returned_output = subprocess.check_output(cmd)
		return_value= return_value+returned_output.decode("utf-8")
	return return_value
app.run()