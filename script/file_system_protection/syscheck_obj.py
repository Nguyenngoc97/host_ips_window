import sys
from win32.src.integrity.integrity_func import *
import json
def ussage():
    print("Ussage")
    print("-[i/d]: insert or delete")
    print("path")
    print("type")
    return 1

def removeFromDb():
    return removeDb()

def main():
    # create_integrity_db()
    argv = sys.argv
    if len(argv) != 4:
        if len(argv) == 3:
            if argv[1] == '-x':
                res = add_sys_check_from_xml(argv[2])
                checkList = get_check_list()
                print({'result': res == 0, 'check_list': checkList})
                return 0
        if(argv[1] == '-l'):
            ret = {"check_list":get_list_sys_check()}
            ret = json.dumps(ret)
            print(ret)
            return 0
        elif argv[1]  == '-a':
            ret = {'alert_list':get_list_alert()}
            ret = json.dumps(ret)
            print(ret)
            return 0
            
        return ussage()

    if argv[1] == '-i':
        res = add_check_object_to_db(argv[2], argv[3])
        checkList = get_check_list()
        print({'result': res == 0, 'check_list': checkList})
        return 0
    elif argv[1] == '-r':
        res = remove_check_object_from_db(argv[2], argv[3])
        checkList = get_check_list()
        print({'result': res == 0, 'check_list': checkList})
        return 0
    elif argv[1] == '-s':
        res = scan(argv[2], argv[3])
        alertList = get_alert_list()
        print({'result': res == 0, 'alert_list': alertList})
        return 0
   
if __name__ == '__main__':
    # removeDb()
    # print(get_list_sys_check())
    main()
    # checkList = get_check_list()
    # print({'result': res == 0, 'check_list': checkList})

