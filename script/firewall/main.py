import os, subprocess
import re
import json
import argparse

WINDOW_FIREWALL_COMMAND = "netsh advfirewall firewall "
SHOW_RULE_COMMAND = WINDOW_FIREWALL_COMMAND+ "show rule name=all"


def get_raw_rules_list():
    raw_rules_list = str(subprocess.check_output(SHOW_RULE_COMMAND))
    rules_list = raw_rules_list.split("\\r\\n\\r\\n")
    return(rules_list)
    # for rule in rules_list[:1]:
    #     print(rule)


def process_raw_rule(rule):
    dict_rule = {}
    rule_split = rule.split("\\r\\n")
    # try:
    #     rule_split.remove(b"")
    #     rule_split.remove(b'----------------------------------------------------------------------') 
    # except:
    #     pass
    for i in rule_split:
        if ":" in i:
            try:
                parameter, value = i.split(":")
                # print(parameter, value.strip())
                dict_rule[str(parameter)] = str(value.strip())
            except Exception as e:
                # print(e, i)
                pass
    return(dict_rule)

def get_dict_rule_list():

    dict_rules_list = []
    rules_list = get_raw_rules_list()
    # print(rules_list[:2])
    for rule in rules_list[:]:
        try:
            dict_rules_list.append(process_raw_rule(rule))
        except Exception as e:
            # print(e)
            pass
    return dict_rules_list


def main():
    # parser = argparse.ArgumentParser(description="""Get linux information package\n
    # Example: 
    # - To get list rule as json:  ./app 
   
    # Author: Nguyen Quoc Khanh
    # Email:  nqkpro96@gmail.com
    # Bach Khoa Cyber Security, Hanoi, 2020
    # """)
    # # parser.add_argument('--upgradeable', action="store_true", help='check list update package:')
    # # args = parser.parse_args()
    print(json.dumps(get_dict_rule_list(), indent=4))

if __name__ == "__main__":
    main()


    