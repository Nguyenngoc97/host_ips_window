import os
import subprocess
import re
import json
import argparse
import ctypes
WINDOW_FIREWALL_COMMAND = "netsh advfirewall firewall "
SHOW_RULE_COMMAND = WINDOW_FIREWALL_COMMAND+ "show rule name=all"

def checkAdmin():
    try:
        is_admin = os.getuid() == 0
    except AttributeError:
        is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0
    return is_admin
def get_raw_rules_list():
    # Get raw rules from netsh
    raw_rules_list = str(subprocess.check_output(SHOW_RULE_COMMAND, stderr=subprocess.STDOUT,shell=True))
    rules_list = raw_rules_list.split("\\r\\n\\r\\n")
    return(rules_list)
    # for rule in rules_list[:1]:
    #     print(rule)


def process_raw_rule(rule):
    # Process raw rule to human-readable dict type
    dict_rule = {}
    rule_split = rule.split("\\r\\n")
    for i in rule_split:
        if ":" in i:
            try:
                parameter, value = i.split(":", 1)
                # print(parameter, value.strip())
                dict_rule[str(parameter)] = str(value.strip())
            except Exception as e:
                print(e, i)
    return(dict_rule)

def get_dict_rule_list():
# Process raw rules list to human-readable dict type
    dict_rules_list = []
    rules_list = get_raw_rules_list()
    # print(rules_list[:2])
    for rule in rules_list[:]:
        try:
            dict_rules_list.append(process_raw_rule(rule))
        except Exception as e:
            print(e)
    return dict_rules_list

def fromDictMakeRuleCommand(dict_rule):
    #  From user input make rule
    string_rule = ""
    if  dict_rule["Rule_Name"] != None:
        string_rule += " name=\"" + dict_rule["Rule_Name"] + "\""
    if  dict_rule["LocalIP"] != None:
        string_rule += " localip=" + dict_rule["LocalIP"]
    if  dict_rule["RemoteIP"] != None:
        string_rule += " remoteip=" + dict_rule["RemoteIP"]
    if  dict_rule["LocalPort"] != None:
        string_rule += " localport=" + dict_rule["LocalPort"]     
    if  dict_rule["RemotePort"] != None:
        string_rule += " remoteport=" + dict_rule["RemotePort"]     
    if  dict_rule["Protocol"] != None:
        string_rule += " protocol=" + dict_rule["Protocol"]   
    if  dict_rule["Action"] != None:
        string_rule += " action=" + dict_rule["Action"]
    if  dict_rule["Direction"] != None:
        string_rule += " dir=" + dict_rule["Direction"]
    if  dict_rule["Program"] != None:
        string_rule += " program=\"" + dict_rule["Program"] + "\""
    # if  dict_rule["Interface"] != None:
    #     string_rule += " action=" + dict_rule["Interface"] 
    # if  dict_rule["interfacetype"] != None:
    #     string_rule += " program=" + dict_rule["Program"]  
    return(string_rule) 

def appendRule(string_rule):
    cmd = WINDOW_FIREWALL_COMMAND + "add rule "+ string_rule
    print(cmd)
    res = subprocess.run(cmd.split(), capture_output=True)
    return(json.dumps({'returncode':str(res.returncode),'stderr':str(res.stderr), 'stdout':str(res.stdout ) }))

def deleteRule(string_rule):
    cmd = WINDOW_FIREWALL_COMMAND + "delete rule "+ string_rule
    print(cmd)
    res = subprocess.run(cmd.split(), capture_output=True)
    return(json.dumps({'returncode':str(res.returncode),'stderr':str(res.stderr), 'stdout':str(res.stdout ) }))

def setRule(string_rule):
    cmd = WINDOW_FIREWALL_COMMAND + "set rule "+ string_rule
    print(cmd)
    # return str(subprocess.check_output(cmd, stderr=subprocess.STDOUT,shell=True))
    res = subprocess.run(cmd.split(), capture_output=True)
    return(json.dumps({'returncode':str(res.returncode),'stderr':str(res.stderr), 'stdout':str(res.stdout ) }))

    
    
    


def main():
    parser = argparse.ArgumentParser(description="""Window Firewall api\n
    Example: 
    - To get list rule as json:  ./window_firewall -L
   
    Author: Nguyen Quoc Khanh
    Email:  nqkpro96@gmail.com
    Bach Khoa Cyber Security, Hanoi, 2020
    Example:
    -A --protocol udp -d out -n TEST1 -J block --src-ip  0.0.0.0 --dst-ip 1.1.1.1 --src-port 70 --dst-port 80
    Add rule for protocol UDP, name TEST1, for blocking all package from 0.0.0.0:70 go out to 1.1.1.1:80
    -n block8888 --disable 
    Block rule name block8888
    """)
    parser.add_argument('-L','--list-rule', action="store_true", help='Get rules list JSON.')
    parser.add_argument('-A','--append-rule', action="store_true", help='Add rule.')
    parser.add_argument('-D','--delete-rule', action="store_true", help='Delete rule.')
    parser.add_argument('--enable-rule', action="store_true", help='Disable rule.')
    parser.add_argument('--disable-rule', action="store_true", help='Enable rule.') 
    parser.add_argument('--protocol', type=str, choices=['any', 'tcp', 'udp','icmpv4','icmpv6'],help='Protocol, tcp|udp|any|icmpv4|icmpv6| (default=any)')
    parser.add_argument('--src-ip', type=str, help='IP range source')
    parser.add_argument('--dst-ip', type=str, help='IP range destination')
    parser.add_argument('--src-port', type=str, help='Port range source 0-65535')
    parser.add_argument('--dst-port', type=str, help='Port range destination 0-65535')
    parser.add_argument('--rule-name','-n', type=str,   help='Name of the rule')
    parser.add_argument('--rule-target','-J', type=str, choices=['block', 'allow', 'bybass'], help='Action of the rule')
    parser.add_argument('--direction','-d', type=str, choices=['in','out'],  help='Inboud or Outbound Rule')
    parser.add_argument('--program', type=str, help='Rule for program')
    args = parser.parse_args()

    if args.list_rule:
        print(json.dumps(get_dict_rule_list(), indent=4))
    else:
        dict_rule = {
            "Rule_Name":args.rule_name,
            "LocalIP":args.src_ip,
            "RemoteIP":args.dst_ip,
            "Protocol":args.protocol,
            "LocalPort":args.src_port,
            "RemotePort":args.dst_port,
            "Action":args.rule_target,
            "Direction":args.direction,
            "Program":args.program
        }
    if checkAdmin() == 0:
        print("Require Administrator rights to add/del rules")
        # exit()
    if args.append_rule:
        print("Add rule:", appendRule(fromDictMakeRuleCommand(dict_rule)))
        exit(0)
    elif args.delete_rule:
        dict_rule["Action"] = None
        print("Delete rule:", deleteRule(fromDictMakeRuleCommand(dict_rule)))
        exit(0)
    elif args.enable_rule:
        dict_rule["Action"] = None
        print("Set rule:", setRule(fromDictMakeRuleCommand(dict_rule)+" new enable=yes"))
        exit(0)
    elif args.disable_rule:
        dict_rule["Action"] = None
        print("Set rule:", setRule(fromDictMakeRuleCommand(dict_rule)+" new enable=no"))
        exit(0)
if __name__ == "__main__":
    main()

# test -A --protocol udp -n TEST1 -J block --src-ip  0.0.0.0 --dst-ip 1.1.1.1 --src-port 70 --dst-port 80
 


# Usage: add rule name=<string>
#       dir=in|out
#       action=allow|block|bypass
#       [program=<program path>]
#       [service=<service short name>|any]
#       [description=<string>]
#       [enable=yes|no (default=yes)]
#       [profile=public|private|domain|any[,...]]
#       [localip=any|<IPv4 address>|<IPv6 address>|<subnet>|<range>|<list>]  
#       [remoteip=any|localsubnet|dns|dhcp|wins|defaultgateway|
#          <IPv4 address>|<IPv6 address>|<subnet>|<range>|<list>]
#       [localport=0-65535|<port range>[,...]|RPC|RPC-EPMap|IPHTTPS|any (default=any)]
#       [remoteport=0-65535|<port range>[,...]|any (default=any)]
#       [protocol=0-255|icmpv4|icmpv6|icmpv4:type,code|icmpv6:type,code|     
#          tcp|udp|any (default=any)]
#       [interfacetype=wireless|lan|ras|any]
#       [rmtcomputergrp=<SDDL string>]
#       [rmtusrgrp=<SDDL string>]
#       [edge=yes|deferapp|deferuser|no (default=no)]
#       [security=authenticate|authenc|authdynenc|authnoencap|notrequired    
#          (default=notrequired)]

# Remarks:

#       - Add a new inbound or outbound rule to the firewall policy.
#       - Rule name should be unique and cannot be "all".
#       - If a remote computer or user group is specified, security must be  
#         authenticate, authenc, authdynenc, or authnoencap.
#       - Setting security to authdynenc allows systems to dynamically       
#         negotiate the use of encryption for traffic that matches
#         a given Windows Defender Firewall rule. Encryption is negotiated based on
#         existing connection security rule properties. This option
#         enables the ability of a machine to accept the first TCP
#         or UDP packet of an inbound IPsec connection as long as
#         it is secured, but not encrypted, using IPsec.
#         Once the first packet is processed, the server will
#         re-negotiate the connection and upgrade it so that
#         all subsequent communications are fully encrypted.
#       - If action=bypass, the remote computer group must be specified when 
# dir=in.
#       - If service=any, the rule applies only to services.
#       - ICMP type or code can be "any".
#       - Edge can only be specified for inbound rules.
#       - AuthEnc and authnoencap cannot be used together.
#       - Authdynenc is valid only when dir=in.
#       - When authnoencap is set, the security=authenticate option becomes an
#         optional parameter.

# Examples:

#       Add an inbound rule with no encapsulation security for browser.exe:  
#       netsh advfirewall firewall add rule name="allow browser"
#       dir=in program="c:\programfiles\browser\browser.exe"
#       security=authnoencap action=allow

#       Add an outbound rule for port 80:
#       netsh advfirewall firewall add rule name="allow80"
#       protocol=TCP dir=out localport=80 action=block

#       Add an inbound rule requiring security and encryption
#       for TCP port 80 traffic:
#       netsh advfirewall firewall add rule
#       name="Require Encryption for Inbound TCP/80"
#       protocol=TCP dir=in localport=80 security=authdynenc
#       action=allow

#       Add an inbound rule for browser.exe and require security
#       netsh advfirewall firewall add rule name="allow browser"
#       dir=in program="c:\program files\browser\browser.exe"
#       security=authenticate action=allow

#       Add an authenticated firewall bypass rule for group
#       acmedomain\scanners identified by a SDDL string:
#       netsh advfirewall firewall add rule name="allow scanners"
#       dir=in rmtcomputergrp=<SDDL string> action=bypass
#       security=authenticate

#       Add an outbound allow rule for local ports 5000-5010 for udp-        
#       Add rule name="Allow port range" dir=out protocol=udp localport=5000-5010 action=allow
