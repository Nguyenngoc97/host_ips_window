import subprocess

import socket
import fcntl
import struct

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

def netinfo():
	cmd = "ip link show | cut -d: -f2,2"+"| awk -F"+'" "'+" '{ printf("+'"%-5s|||"'+", $0); }'"
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	p_status = p.wait()
	print(output)

	net = [];
	i =0


	data = str(output).split("|||")
	for data in data:
		i = i +1
		if(i % 2 == 0):
			continue
		net.append(data.strip())

	net = net[:-1]
	# print(net)

	for net in net:
		print(net)
		try:
			ip = get_ip_address(net)
			print(ip)
			print("UP")
		except:
			print("None")
			print("DOWN")
netinfo()
# print(data[1].split('||')[1])
